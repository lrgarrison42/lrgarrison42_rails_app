$('document').ready(function(){
    if($('#workList').length)
    {
        
        var myWorkList = document.getElementById("workList").getElementsByTagName("tr");
        
        for (var i = 0; i < myWorkList.length; i++)
        {
            myWorkList[i].addEventListener("click", activateWorkItem);
        }
    }
    
    if($('#courseList').length)
    {
        
        var myCourseList = document.getElementById("workList").getElementsByTagName("tr");
        
        for (var i = 0; i < myCourseList.length; i++)
        {
            myCourseList[i].addEventListener("click", activateCourseItem);
        }
    }
    
    if($('#skillList').length)
    {
        
        var mySkillList = document.getElementById("skillList").getElementsByTagName("tr");
        
        for (var i = 0; i < mySkillList.length; i++)
        {
            mySkillList[i].addEventListener("click", activateSkillItem);
        }
    }
    
    if($('#refList').length)
    {
        
        var myRefList = document.getElementById("refList").getElementsByTagName("tr");
        
        for (var i = 0; i < myRefList.length; i++)
        {
            myRefList[i].addEventListener("click", activateRefItem);
        }
    }
    
    if($('#honorList').length)
    {
        
        var myHonorList = document.getElementById("honorList").getElementsByTagName("tr");
        
        for (var i = 0; i < myHonorList.length; i++)
        {
            myHonorList[i].addEventListener("click", activateHonorItem);
        }
    }
    
    function activateWorkItem()
    {
        var name = this.childNodes[7].innerHTML;
        var job = this.childNodes[9].innerHTML;
        
        document.getElementById("myModalLabel").innerHTML = name;
        document.getElementById("myModalBody").innerHTML = job;
    }
    
    function activateCourseItem()
    {
        var name = this.childNodes[3].innerHTML;
        var job = this.childNodes[5].innerHTML;
        
        document.getElementById("myModalLabel").innerHTML = name;
        document.getElementById("myModalBody").innerHTML = job;
    }
    
    function activateSkillItem()
    {
        var name = this.childNodes[3].innerHTML;
        var job = this.childNodes[5].innerHTML;
        
        document.getElementById("myModalLabel").innerHTML = name;
        document.getElementById("myModalBody").innerHTML = job;
    }
    
    function activateRefItem()
    {
        var name = this.childNodes[3].innerHTML;
        var job = this.childNodes[5].innerHTML;
        
        document.getElementById("myModalLabel").innerHTML = name;
        document.getElementById("myModalBody").innerHTML = job;
    }
    
    function activateHonorItem()
    {
        var name = this.childNodes[5].innerHTML;
        var job = this.childNodes[7].innerHTML;
        
        document.getElementById("myModalLabel").innerHTML = name;
        document.getElementById("myModalBody").innerHTML = job;
    }
    
});