class DashboardController < ApplicationController
  def index
    if user_signed_in?
     @user = User.find(current_user.id)
     @courses = Course.where("owner = ?", current_user.id)
     @works = Work.where("owner = ?", current_user.id)
     @honors = Honor.where("owner = ?", current_user.id)
     @skills = Skill.where("owner = ?", current_user.id)
     @references = Reference.where("owner = ?", current_user.id)
    end
  end

  def page1
  end

  def page2
      @user = User.find(current_user.id)
      @courses = Course.where("owner = ?", current_user.id)
  end
  
  def page3
    @user = User.find(current_user.id)
    @works = Work.where("owner = ?", current_user.id)
  end
  
  def page4
    @user = User.find(current_user.id)
    @honors = Honor.where("owner = ?", current_user.id)
  end
  
  def page5
    @user = User.find(current_user.id)
    @skills = Skill.where("owner = ?", current_user.id)
  end
  
  def page6
    @user = User.find(current_user.id)
    @references = Reference.where("owner = ?", current_user.id)
  end
  
  def page7
    user = User.find(current_user.id)
    @experiences = Work.where("owner = ?", current_user.id)
    @skills = Skill.where("owner = ?", current_user.id)
    @honors = Honor.where("owner = ?", current_user.id)
    @references = Reference.where("owner = ?", current_user.id)
    
    Caracal::Document.save '/public/Resume.docx' do |docx|
      
    docx.style do
      id 'Body'
      name 'body'
      font 'Times New Roman'
      size 18
    end
    
    docx.h2 'User Info'
    docx.p do
      style 'Body'
      text 'Name: '
      text user.first_name
      text ' '
      text user.last_name
      br
      text 'Major: '
      text user.major
      br
    end
    
    
    docx.h2 'Experience'
    @experiences.each do | experience |
      docx.p do
        style 'Body'
        text experience.name
        text ": "
        text experience.description
        br
      end
    end
    
    docx.h2 'Skills'
    @skills.each do | skill |
      docx.p do
        style 'Body'
        text skill.skill_name
        text ": "
        text skill.skill_description
        br
      end
    end
    
    docx.h2 'Honors'
    @honors.each do | honor |
      docx.p do
        style 'Body'
        text honor.honor_name
        text ": "
        text honor.honor_description
        br
      end
    end
    
    docx.h2 'References'
    @references.each do | reference |
      docx.p do
        style 'Body'
        text reference.reference_name
        text ": "
        text reference.reference_contact_info
        br
      end
    end
  end
  
  path = File.join(Rails.root, "public")
  send_file(File.join(path, "Resume.docx"))
  
    end
end
