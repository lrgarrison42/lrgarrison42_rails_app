json.extract! work, :id, :owner, :start_date, :end_date, :name, :description, :created_at, :updated_at
json.url work_url(work, format: :json)