Rails.application.routes.draw do
  resources :references
  resources :skills
  resources :honors
  resources :works
  resources :courses
  devise_for :users
  get 'dashboard/index'

  get 'dashboard/page1'

  get 'dashboard/page2'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "dashboard#index"
  get 'dashboard/index'
  get 'dashboard/page1'
  get 'dashboard/page2'
  get 'dashboard/page3'
  get 'dashboard/page4'
  get 'dashboard/page5'
  get 'dashboard/page6'
  get 'dashboard/page7'
end
