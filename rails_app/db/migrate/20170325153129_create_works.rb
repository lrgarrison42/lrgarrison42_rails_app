class CreateWorks < ActiveRecord::Migration[5.0]
  def change
    create_table :works do |t|
      t.integer :owner
      t.string :start_date
      t.string :end_date
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
